require('./bootstrap');

window.Vue = require('vue');

Vue.component('balance', require('./components/balance.vue'));

Vue.component('new-income', require('./components/incomes/newIncome.vue'));
Vue.component('income-row', require('./components/incomes/incomeRow.vue'));
Vue.component('incomes', require('./components/incomes/incomes.vue'));


Vue.component('new-expense', require('./components/expenses/newExpense.vue'));
Vue.component('expense-row', require('./components/expenses/expenseRow.vue'));
Vue.component('expenses', require('./components/expenses/expenses.vue'));


const app = new Vue({
    el: '#app'
});
