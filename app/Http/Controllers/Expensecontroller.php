<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expense;
class ExpenseController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return Expense::where('user_id', auth()->id())->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Expense = new Expense();
        $Expense->conceptExpense = $request->conceptExpense;
        $Expense->quantityExpense = $request->quantityExpense;
        $Expense->user_id = auth()->id();
        $Expense->save();
        return $Expense;
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Expense = Expense::find($id);
        $Expense->conceptExpense = $request->conceptExpense;
        $Expense->quantityExpense = $request->quantityExpense;        
        $Expense->save();
        return $Expense;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Expense = Expense::find($id);
        $Expense->delete();
    }
}
