<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Income;
class incomeController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return Income::where('user_id', auth()->id())->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $income = new Income();
        $income->conceptIncome = $request->conceptIncome;
        $income->quantityIncome = $request->quantityIncome;
        $income->user_id = auth()->id();
        $income->save();
        return $income;
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $income = Income::find($id);
        $income->conceptIncome = $request->conceptIncome;
        $income->quantityIncome = $request->quantityIncome;        
        $income->save();
        return $income;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $income = Income::find($id);
        $income->delete();
    }
}
